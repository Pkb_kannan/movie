package com.auto.movielist.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.auto.movielist.databinding.MovieListItemBinding
import com.auto.movielist.model.ResultMovie


class MovieListAdapter(private val viewModel: MovieListViewModel,
                       private var items: List<ResultMovie>?
)
    : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MovieListItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = items!!.size




    fun setMovie(movie: List<ResultMovie>?) {
        this.items = movie
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: MovieListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(
            viewModel: MovieListViewModel, position: Int?
        ) {

            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, position)
            binding.executePendingBindings()
        }
    }

}

