package com.auto.movielist.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.auto.movielist.R
import com.auto.movielist.customView.RecyclerItemClickListenr
import com.auto.movielist.databinding.DetailsBinding
import com.auto.movielist.databinding.MovieListBinding
import com.auto.movielist.model.ResultMovie as ResultMovie1

class MovieList : AppCompatActivity() {

    var viewmodel: MovieListViewModel? = null
    var binding: MovieListBinding? = null
    var bindingDetails: DetailsBinding? = null
    private var MovieList: MutableLiveData<List<ResultMovie1>> = MutableLiveData<List<ResultMovie1>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_list)
        binding = DataBindingUtil.setContentView(this, R.layout.movie_list)
        viewmodel = ViewModelProviders.of(this).get(MovieListViewModel::class.java)
        binding?.viewModel = viewmodel

        initObservables()
    }

    private fun initObservables() {

            viewmodel?.listMovie()?.observe(
            this,
            Observer<List<ResultMovie1>?> { MovieListModel ->
                if (MovieListModel!!.size == 0) {
                } else {
                    binding?.recyList?.layoutManager = GridLayoutManager(applicationContext,1)
                    binding?.recyList?.adapter = MovieListAdapter(viewmodel!!, MovieListModel
                    )
                    MovieList.value = MovieListModel
                    viewmodel!!.setMoviesInAdapter(MovieListModel)
                }
            })


        this.let {
            RecyclerItemClickListenr(it, binding!!.recyList, object : RecyclerItemClickListenr.OnItemClickListener {

                override fun onItemClick(view: View, position: Int) {
                    //do your work here..

                    DetailsDialod(MovieList.value!!.get(position))

                }

                override fun onItemLongClick(view: View?, position: Int) {
                    TODO("do nothing")
                }
            })
        }?.let { binding?.recyList?.addOnItemTouchListener(it) }

    }

    fun DetailsDialod(value: ResultMovie1) {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewmodel = ViewModelProviders.of(this).get(MovieListViewModel::class.java)
        bindingDetails = DataBindingUtil.inflate(
            LayoutInflater.from(dialog.context),
            R.layout.details,
            null,
            false
        )
        bindingDetails?.root?.let { dialog.setContentView(it) }
        bindingDetails?.viewModel = viewmodel

        bindingDetails?.txtTile?.text = value.title
        bindingDetails?.discription?.text = value.openingCrawl
        bindingDetails?.txtDirector?.text = "Director : "+value.director+"  "
        bindingDetails?.txtProducer?.text = "Producer : "+value.director+"  "
        bindingDetails?.txtDate?.text = "Release date : "+value.releaseDate+"  "

        bindingDetails?.close?.setOnClickListener {
            dialog.dismiss()
        }

        dialog?.show()
    }
}
