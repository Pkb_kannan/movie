package net.gahfy.mvvmposts.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.auto.movielist.component.DaggerViewModelInjector
import com.auto.movielist.component.ViewModelInjector
import com.auto.movielist.ui.MovieListViewModel
import net.gahfy.mvvmposts.injection.module.NetworkModule


abstract class BaseViewModel(application: Application) : AndroidViewModel(application){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MovieListViewModel -> injector.inject(this)

        }
    }
}