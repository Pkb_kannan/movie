
package com.auto.movielist.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class MovieListModel {

    @SerializedName("count")
    private Long mCount;
    @SerializedName("next")
    private Object mNext;
    @SerializedName("previous")
    private Object mPrevious;
    @SerializedName("results")
    private List<ResultMovie> mResults;

    public Long getCount() {
        return mCount;
    }

    public void setCount(Long count) {
        mCount = count;
    }

    public Object getNext() {
        return mNext;
    }

    public void setNext(Object next) {
        mNext = next;
    }

    public Object getPrevious() {
        return mPrevious;
    }

    public void setPrevious(Object previous) {
        mPrevious = previous;
    }

    public List<ResultMovie> getResults() {
        return mResults;
    }

    public void setResults(List<ResultMovie> results) {
        mResults = results;
    }

}
