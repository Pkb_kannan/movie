package com.auto.movielist.component

import com.auto.movielist.ui.MovieListViewModel
import dagger.Component
import net.gahfy.mvvmposts.injection.module.NetworkModule
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param movieListViewModel movieListViewModel in which to inject the dependencies
     */
    fun inject(movieListViewModel: MovieListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}