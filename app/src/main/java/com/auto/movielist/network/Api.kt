package com.mcr.active.network

import com.auto.movielist.model.MovieListModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


/**
 * The interface which provides methods to get result of webservices
 */
interface Api {
    /**
     * Get the list of the pots from the API
     */

    @GET("films/")
    fun getMovies(): Call<MovieListModel>

}