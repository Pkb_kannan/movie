package com.auto.movielist.ui

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.auto.movielist.model.MovieListModel
import com.auto.movielist.model.ResultMovie
import com.auto.movielist.util.SingleLiveEvent
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mcr.active.network.Api
import net.gahfy.mvvmposts.base.BaseViewModel
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class MovieListViewModel (application: Application) : BaseViewModel(application) {
    @Inject
    lateinit var api: Api

    val adapter : MovieListAdapter? = null
    var loadComplect: SingleLiveEvent<Boolean>? = null
    var isProgress: ObservableInt = ObservableInt(View.GONE)
    private var MovieList: MutableLiveData<List<ResultMovie>> = MutableLiveData<List<ResultMovie>>()

    init {

        loadComplect = SingleLiveEvent<Boolean>()
        getMovies()

    }

    fun listMovie(): MutableLiveData<List<ResultMovie>> {
        return MovieList
    }

    fun getMovies(){
//        launchFindMapFragment?.value = true
//
        loadComplect?.value = false
        isProgress.set(View.VISIBLE)
        val call = api.getMovies()
        call.enqueue(object : retrofit2.Callback<MovieListModel>
        {
            override fun onFailure(call: Call<MovieListModel>?, t: Throwable?) {
                Log.e("response", t?.message)
                isProgress.set(View.GONE)
            }

            override fun onResponse(
                call: Call<MovieListModel>?,
                response: Response<MovieListModel>?
            ) {

                val gson = Gson()
                val jsonElement: JsonElement = gson.toJsonTree(response?.body())
                Log.e("Location_details_value", jsonElement.toString())

                isProgress.set(View.GONE)
                MovieList?.value = response?.body()?.results
                loadComplect?.value = true
            }

        })
    }

    fun setMoviesInAdapter(movie: List<ResultMovie>?) {
        adapter?.setMovie(movie)
        adapter?.notifyDataSetChanged()
    }

//    fun moviwNmae(position : Int){
//
//        return MovieList.value(position)?.
//
//    }

    fun moviwNmae(index: Int?): String? {
        return if (MovieList.getValue() != null && index != null && MovieList.getValue()!!.size > index
        ) {
            MovieList.getValue()!!.get(index).title
        } else null
    }

}